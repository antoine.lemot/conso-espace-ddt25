Application R-Shiny de visualisation de l'artificialisation par le bâti dans les communes et territoires du Doubs.
Réalisée par le Cerema pour la DDT du Doubs dans le cadre de l'observatoire départemental de consommation d'espaces.

Ce dépot contient tout le code source de l'application à l'exception de la base de données qui est lue au lancement de l'application.

La dernière version est la version 2024 publiée le 6 août 2024

L'application est publiée à l'adresse [https://cerema-med.shinyapps.io/conso_espace_doubs_ed2024/](https://cerema-med.shinyapps.io/conso_espace_doubs_ed2024/)
